public with sharing class DemoClass2 {
    /**
     * An empty constructor for the testing
     */
    public DemoClass2() {}

    /**
     * Get the version of the SFDX demo app
     */
    public String getAppVersion() {
        system.debug('16May test2');
        return '1.0.0';
    }
}
